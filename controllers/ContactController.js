var ContactService = require ('../services/ContactService');

const ContactController = {

    post(req,res) {
        ContactService.create(req,res);
    },

    fetchAll (req,res) {
        ContactService.all(req,res);
    },

    delete (req,res) {
        ContactService.delete(req,res);
    },
}
module.exports = ContactController;