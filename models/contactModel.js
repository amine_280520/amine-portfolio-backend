const mongoose = require('mongoose');

const contactSchema = new mongoose.Schema({

    objectId : {
        type: mongoose.Types.ObjectId,
    },
    fullname : {
        type : String,
        required : true,
        trim : true
    },
    subject : {
        type : String,
        required : true,
        trim : true
    },
    email : {
        type : String,
        required : true,
        trim : true
    },
    message : {
        type : String,
        required : true,
        trim : true
    },
});

const Contact = mongoose.model("contact",contactSchema);
module.exports = Contact;