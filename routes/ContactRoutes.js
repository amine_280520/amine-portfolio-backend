const router = require('express').Router();
const ContactController = require ('../controllers/ContactController');

router.post('/create', ContactController.post);
router.get('/all', ContactController.fetchAll);
router.delete('/delete/:id', ContactController.delete);

module.exports = router;