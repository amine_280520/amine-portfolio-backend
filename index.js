require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const mongoose = require ('mongoose');

const connection_to_my_database= process.env.CONNECTION_TO_DATABASE;

mongoose.set('strictQuery', true);

//Enable Cors
app.use(
  cors()
);

//Connect to database
//'mongodb+srv://amine2805:Syrine2805@cluster0.dqckmkg.mongodb.net/test?authSource=admin&replicaSet=atlas-2a2dn1-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true
mongoose.connect(connection_to_my_database,{
  useNewUrlParser : true,
  useUnifiedTopology: true,
}, (err)=> {
  if(err) throw err;
  console.log("MongoDB connection established");
});

app.use(express.json());
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));


app
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'ejs')
  .get('/', (req, res) => res.render('pages/index'))
  .listen(process.env.PORT || 5000, () => console.log(`Listening on ${process.env.PORT}`))

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader("Access-Control-Allow-Headers", "*");
  res.setHeader('Access-Control-Allow-Headers', '*');

  next();
});

//Main Route

app.use('/api', require('./MainRoutes'));

//Listening Port

app.listen(process.env.PORT || 5000,()=>{
    console.log('Backend Server is Running');
});

module.exports = app;